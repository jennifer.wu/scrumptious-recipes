from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

# from django.views.generic.edit import CreateView, UpdateView, DeleteView

from tags.models import Tag


# Create your views here.


class TagListView(ListView):
    model = Tag
    template_name = "tags/list.html"


class TagDetailView(DetailView):
    model = Tag
    template = "tags/detail.html"
